DefinitionBlock ("", "SSDT", 2, "hack", "IMEI", 0x00000000)
{
    External(\_SB.PCI0, DeviceObj) // IMEI 


    Scope(\_SB.PCI0)
    {

        Device(IMEI) // OptiPlex 980
        {
            Name(_ADR, 0x00160000) // _ADR: Address
                Name(IMEI, One)
                    Method(_DSM, 4, NotSerialized) // _DSM: Device-Specific Method
            {
                If(LEqual(Arg2, Zero)) { Return(Buffer(){}) }
                Return(Package(){
                    "built-in", Buffer(){ 0x00 },
                    "model", Buffer(){ "Ibex Peak HECI Controller" },
                    "name", Buffer(){ "5 Series/3400 Series Chipset HECI Controller" },
                    "AAPL,slot-name", Buffer(){ "Built In" },
				   "compatible", Buffer(){ "pci1043,1520" },
                })
            }
        }

        
    }
}


